module.exports = function(grunt) {
    if (grunt.file.exists('.gruntoptions.json')) {
        var pc = grunt.file.readJSON('.gruntoptions.json');
        grunt.config.merge(pc);
    }
};