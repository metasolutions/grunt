module.exports = function(grunt) {
    grunt.registerTask('update', 'Update libraries needed by the application', function() {
        var bowerUpdate = require('bower').commands.update;
        var libs = grunt.config.get('update.libs');
        var done = this.async();
        bowerUpdate(libs)
            .on('log', function(result) {
                grunt.log.writeln(['bower', result.id.cyan, result.message].join(' '));
            })
            .on('error', function(error) {
                grunt.log.writeln(error);
                done(false);
            })
            .on('end', function(updated) {
                done();
            });
    });

    grunt.registerTask('install', 'Install libraries needed by the application', function(){
        var bower = require('bower');
        var renderer = new (require('bower/lib/renderers/StandardRenderer'))('install',{color:true,cwd:process.cwd()});
        var done = this.async();
        bower.commands.install()
            .on('log',function(log){
                renderer.log(log);
            })
            .on('prompt',function(prompts,callback){
                renderer.prompt(prompts).then(function(answer){
                    callback(answer);
                });
            })
            .on('error',function(err){
                renderer.error(err);
                done(false);
            })
            .on('end',function(data){
                renderer.end(data);
                done();
            });
    });
};