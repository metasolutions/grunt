var q = require('q');

var errors = {
    poe: "For communication to work with POEditor an apikey and projectids array must be provided as options inside of the poeditor object, i.e. poeditor.apikey and poeditor.projectids",
    langs: "You need to specify which languages to download, i.e. the nls.langs option."
};

module.exports = function(grunt) {
    var nls = require('../utils/nls.js')(grunt);

    grunt.config.merge({
        nls: {
            referenceDir: "nls/reference/",
            translationDir: "nls/translations/",
            mergeDir: "nls/merged",
            depParentDirs: ["../"],
            depRepositories: [],
            depDir: "libs/",
            langs: ["en", "sv"]
        }
    });

    grunt.registerTask('nls', 'Alias for nlsDev', ['nlsDev']);

    grunt.registerTask('nlsDev', 'Copy strings into merge directory from parallell directories (to be used in development phase)', function() {
        var deps = grunt.config.get('nls.depParentDirs');
        var mergeDir = grunt.config.get('nls.mergeDir');
        var referenceDir = grunt.config.get('nls.referenceDir');
        var translationDir = grunt.config.get('nls.translationDir');
        if (!deps) {
            grunt.fail.fatal(errors.deps);
        }
        if (grunt.file.exists(mergeDir)) {
            grunt.file.delete(mergeDir);
        }
        grunt.file.mkdir(mergeDir);
        nls.copyNLSDeps(deps, mergeDir, ["*/"+referenceDir, "*/"+translationDir]);
    });

    grunt.registerTask('nlsRel', 'Copy strings into merge directory from dependent repositories in libs', function() {
        var referenceDir = grunt.config.get('nls.referenceDir');
        var translationDir = grunt.config.get('nls.translationDir');
        var mergeDir = grunt.config.get('nls.mergeDir');
        var depRepos = grunt.config.get('nls.depRepositories');
        var depDir = grunt.config.get('nls.depDir');

        if (grunt.file.exists(mergeDir)) {
            grunt.file.delete(mergeDir);
        }
        grunt.file.mkdir(mergeDir);

        nls.copyNLSDep("./", mergeDir, [referenceDir, translationDir]);
        for (var i = 0;i<depRepos.length;i++) {
            nls.copyNLSDep(depDir+depRepos[i]+"/", mergeDir, [referenceDir, translationDir]);
        }
    });

    grunt.registerTask('nlsUpload', 'Uploading both terms and english translation (POEditor admins only)',
        ["nlsUploadTerms", "nlsUploadLang:en"]);

    grunt.registerTask('nlsUploadTerms', 'Uploading terms to POEditor (POEditor admins only)', function() {
        var src = grunt.config.get('nls.referenceDir');
        var apikey = grunt.config.get('poeditor.apikey');
        var projId = grunt.config.get('poeditor.projectids');
        if (!apikey || !projId) {
            grunt.fail.fatal(errors.poe);
        } else {
            var terms = nls.readTerms(false, src, false);
            var done = this.async();
            //Sync with poeditor API
            require('../utils/poeclient.js')(apikey, projId[0]).syncTerms(terms).then(function() {
                done();
            });
        }
    });

    var getRemoteTerms = function(langs, f) {
        var promises = [];
        for (var i = 0; i < langs.length; i++) {
            promises.push(f(langs[i]));
        }
        return q.all(promises);
    };

    grunt.registerTask('nlsDownload', 'Downloading and caching translations from POEditor (POEditor admins only)', function() {
        var cache = grunt.config.get('nls.translationDir');
        var langs = grunt.config.get('nls.langs');
        var remoteTestData = grunt.config.get('nls.remoteTestData');
        var apikey = grunt.config.get('poeditor.apikey');
        var projIds = grunt.config.get('poeditor.projectids');
        if (!remoteTestData && (!apikey || !projIds)) {
            grunt.fail.fatal(errors.poe);
        } else if (!langs) {
            grunt.fail.fatal(errors.langs);
        }

        if (grunt.file.exists(cache)) {
            grunt.file.delete(cache);
        }
        grunt.file.mkdir(cache);

        if (remoteTestData) {
            getRemoteTerms(langs, remoteTestData).then(function(terms) {
                nls.writeTranslations(langs, terms, cache);
            });
        } else {
            var promises = [];
            var poeclient = require('../utils/poeclient.js');
            var done = this.async();
            for (var i=0;i<projIds.length;i++) {
                var f = poeclient(apikey, projIds[i]).exportTermsAndDefinitions;
                promises.push(getRemoteTerms(langs, f).then(function(terms) {
                    nls.writeTranslations(langs, terms, cache);
                }));
            }
            q.all(promises).then(function() {
                done();
            });
        }
    });

    grunt.registerTask('nlsUploadLang', 'Upload of translations to POEditor (only for initialization and for POEditor admins only)',
        function(lang) {
            var src = grunt.config.get('nls.referenceDir');
            var apikey = grunt.config.get('poeditor.apikey');
            var projId = grunt.config.get('poeditor.projectids');
            if (!apikey || !projId) {
                grunt.fail.fatal(errors.poe);
            }

            var terms;
            if (typeof lang === "undefined") {
                grunt.fail.fatal("You need to indicate which language to upload, e.g. 'nlsUpload:sv'");
                return;
            } else if (lang === "en") {
                terms = nls.readTerms(false, src, true);
            } else {
                terms = nls.readTerms(true, src + lang, true);
            }

            var done = this.async();
            //Sync with poeditor API
            require('../utils/poeclient.js')(apikey, projId[0]).uploadDefinitions(terms, lang).then(function() {
                done();
            });
        });
};