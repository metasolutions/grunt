module.exports = function(grunt) {
    grunt.task.loadTasks('tasks');
    grunt.loadNpmTasks('grunt-available-tasks');
};