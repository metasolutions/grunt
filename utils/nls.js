var requirejs = require('requirejs');
requirejs.config({
    nodeRequire: require
});

module.exports = function(grunt) {

    var copyNLSDep = function(path, dest, patterns) {
        grunt.file.expand({filter: "isDirectory", cwd: path}, patterns).forEach(function(nlsDir) {
            grunt.file.expandMapping(["**/*.js"], dest, {expand: true, filter: "isFile", cwd: path+nlsDir}).forEach(
                function(m) {
                    grunt.file.copy(m.src[0], m.dest);
                });
        });
    };

    var copyNLSDeps = function(pathDeps, dest, patterns) {
        pathDeps.forEach(function(path) {
            copyNLSDep(path, dest, patterns);
        });
    };

    var readTerms = function(isRoot, path, includeDefs) {
        var nlsFiles = grunt.file.expand({filter: "isFile", cwd: path}, ["*.js"]);
        grunt.log.writeln('Found ' + nlsFiles.length + ' nls files');

        var context, value, res = [];
        for (var i = 0; i < nlsFiles.length; i++) {
            context = nlsFiles[i];
            var obj = requirejs(path + "/" + context);
            context = context.substr(0, context.length - 3); //Remove .js at the end
            grunt.log.writeln('context: ' + context);
            var root = isRoot ? obj : obj.root;
            for (var key in root) if (root.hasOwnProperty(key)) {
                value = root[key];
                grunt.log.writeln('    ' + key + ": " + value);
                if (includeDefs) {
                    res.push({term: key, context: context, definition: value});
                } else {
                    res.push({term: key, context: context});
                }
            }
        }
        return res;
    };

    var getContexts = function(terms) {
        var contexts = {};
        var arr = [];
        for (var t = 0; t < terms.length; t++) {
            var c = terms[t].context;
            if (!contexts[c]) {
                contexts[c] = true;
                arr.push(c);
            }
        }
        return arr;
    };

    var getContextGroupedTerms = function(terms) {
        var grouped = {};
        for (var i = 0; i < terms.length; i++) {
            var term = terms[i];
            var context = term.context;
            if (context) {
                if (!grouped[context]) {
                    grouped[context] = {};
                }
                grouped[context][term.term] = term.definition;
            }
        }
        return grouped;
    };

    var termsToDefine = function(terms) {
        return "define(" + JSON.stringify(terms || {}, null, "  ") + ");";
    };

    var termsToRootDefine = function(terms, langs) {
        var obj = {root: terms || {}};
        for (var i = 0; i < langs.length; i++) {
            obj[langs[i]] = true;
        }

        return "define(" + JSON.stringify(obj, null, "  ") + ");";
    };

    var delNull = function(terms) {
        for (var k in terms) if (terms.hasOwnProperty(k)) {
            if (terms[k] === null) {
                delete terms[k];
            }
        }
        return terms;
    };

    var writeLang = function(path, lang, terms, contexts) {
        var grouped = getContextGroupedTerms(terms);
        for (var i = 0; i < contexts.length; i++) {
            var c = contexts[i];
            grunt.file.write(path + lang + "/" + c + ".js", termsToDefine(delNull(grouped[c])));
        }
    };

/*    var writeRoot = function(path, terms, contexts, langs) {
        var grouped = getContextGroupedTerms(terms);
        for (var i = 0; i < contexts.length; i++) {
            var c = contexts[i];
            grunt.file.write(path + c + ".js", termsToRootDefine(delNull(grouped[c]), langs));
        }
    };*/

    var writeTranslations = function(langs, terms, dest) {
        var enIdx = langs.indexOf("en");
        var enTerms = terms[enIdx];
        terms.splice(enIdx, 1);
        langs.splice(enIdx, 1);
        var contexts = getContexts(enTerms);
        for (var l = 0; l < langs.length; l++) {
            writeLang(dest, langs[l], terms[l], contexts);
        }
    };

    return {
        readTerms: readTerms,
        writeTranslations: writeTranslations,
        copyNLSDep: copyNLSDep,
        copyNLSDeps: copyNLSDeps
    };
};